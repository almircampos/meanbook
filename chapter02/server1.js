var http = require('http');

var server = http.createServer( function( requ, res){
	res.writeHead( 200,{
		'Content-Type':'html/plain'
	});
	res.write( 'Hello World - 1' );
	res.end();
});

server.listen( 3000 );

console.log( 'Server running at http://localhost:3000/');