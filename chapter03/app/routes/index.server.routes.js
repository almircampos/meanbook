module.exports = function( app ){ //Note the single argument here: app
	var index = require('../controllers/index.server.controller');
	app.get( '/', index.render );	
};