var config = require( './config')
	exprex = require( 'express' ),
	morgan = require('morgan'),
	compress = require('compression'),
	bodyParser = require( 'body-parser'),
	methodOverride = require( 'method-override')
	session = require( 'express-session');

module.exports = function(){
	var app = exprex();

	if ( process.env.NODE_ENV === 'development'){
		app.use( morgan('dev') );
	} else if ( process.env.NODE_ENV === 'production' ) {
		console.log( "----->>>>> ATTENTION! YOU'RE ON THE PRODUCTION SERVER <<<<<-----");
		app.use( compress() );
	}

	app.use( bodyParser.urlencoded({ extended: true}));
	app.use( bodyParser.json() );
	app.use( methodOverride() );
	app.use( session({
		saveUninitialized: true,
		resave: true,
		secret: config.sessionSecret
	}));

	app.set( 'views', './app/views');
	app.set( 'view engine', 'ejs');

	require( '../app/routes/index.server.routes.js')(app);

	app.use( exprex.static( "./public") );

	return app;	
};